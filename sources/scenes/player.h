#pragma once

#include <simulant/simulant.h>
#include "heid.h"

class Player{
    public:
        Player(smlt::behaviours::RigidBodySimulation* rigidPhys, smlt::behaviours::PhysicsMaterial& phys, smlt::default_init_ptr<smlt::Stage>& stage);
	    smlt::MeshPtr mesh_id;
	    smlt::ActorPtr actor;
        int speed = 5000;
        float turn_speed = 30000.0f;
        int sensitivity = 10;
        smlt::behaviours::RigidBody* controller;
        bool can_jump;
        Heid heid;
};