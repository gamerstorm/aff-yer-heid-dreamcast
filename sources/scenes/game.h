#pragma once

#include <simulant/simulant.h>
#include "player.h"

class GameScene : public smlt::PhysicsScene<GameScene> {
public:
    // Boilerplate
    GameScene(smlt::Window* window):
        smlt::PhysicsScene<GameScene>(window) {}
    
    smlt::behaviours::RigidBodySimulation* m_simulation = nullptr;

    const float GRAVITY = 3000.0f;
    float gravity = GRAVITY;
    smlt::CameraPtr camera;
    Player* player = nullptr;

    float speed = 0.0f;
    float hSpeed = 0.0f;
    float turnSpeed = 0.0f;
    float pitch_ = 0.0f;

    void load();
    void update(float dt);
    void fixed_update(float dt);
    void activate();
    void deactivate();
    void unload();
};
