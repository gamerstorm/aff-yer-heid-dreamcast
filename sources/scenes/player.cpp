#include "player.h"

using namespace smlt;

Player::Player(smlt::behaviours::RigidBodySimulation* rigidPhys, smlt::behaviours::PhysicsMaterial& physMaterial, smlt::default_init_ptr<smlt::Stage>& stage)
{
    mesh_id = stage->assets->new_mesh_from_file("models/cameraman.obj");
    actor = stage->new_actor_with_mesh(mesh_id);
    actor->set_visible(false);
    
    assert(rigidPhys != nullptr);
    controller = actor->new_behaviour<smlt::behaviours::RigidBody>(rigidPhys);

    controller->add_capsule_collider(actor->aabb().height(), actor->aabb().width(), physMaterial);
    controller->add_sphere_collider(actor->aabb().height(), physMaterial);

    controller->move_to(smlt::Vec3(0, 2, 0));
    controller->lock_rotation(false, true, false);
    controller->set_linear_damping(50.0f);
    controller->set_angular_damping(20.0f);
    controller->set_mass(1.0f);
} 