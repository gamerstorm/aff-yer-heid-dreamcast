
#include "game.h"

using namespace smlt;

void GameScene::load() {
    // smlt::behaviours::RigidBodySimulation physics = RigidBodySimulation::create();
    // smlt::behaviours::RigidBodySimulation* m_simulation = smlt::behaviours::RigidBodySimulation::create();
    auto stage = new_stage();  // Create a stage
    camera = stage->new_camera();  // Create a camera within the stage
    auto pipeline = compositor->render(stage, camera);  // Create your pipeline
    link_pipeline(pipeline);

    stage->new_light_as_directional(smlt::Vec3(1, -1, 0));
    stage->set_ambient_light(smlt::Colour(1.0, 1.0, 1.0, 1.0));

    auto level = stage->assets->new_mesh_from_file("models/levels/level1.obj");
    auto level_ = stage->new_actor_with_mesh(level);
    auto c = level_->new_behaviour<behaviours::StaticBody>(physics);
    auto physMat = behaviours::PhysicsMaterial();
    physMat.bounciness = 0.05f;
    physMat.friction = 1.5f;
    physMat.density = 100000.0f;
    c->add_mesh_collider(level->id(), physMat);

    //player->controller->add_capsule_collider(player->actor->aabb().height(), 1, behaviours::PhysicsMaterial::IRON);

    auto physMatIce = behaviours::PhysicsMaterial();
    physMatIce.bounciness = 0.0f;
    physMatIce.friction = 1.5f;
    physMatIce.density = 0.1f;

    player = new Player(physics.get(), physMatIce, stage);

    camera->set_parent(player->actor);
    //auto f = camera->new_behaviour<smlt::behaviours::SmoothFollow>();
    //f->set_target(player->actor);
    //camera->new_behaviour<smlt::behaviours::Fly>(window);
    camera->rotate_to(player->controller->rotation());
    camera->rotate_global_y_by(Degrees(180));
    camera->move_to(0, 6, -1);

    auto y_button = input->new_axis("Forward");
    y_button->set_positive_joystick_button(JOYSTICK_BUTTON_Y);
    auto y_button_kbd = input->new_axis("Forward");
    y_button_kbd->set_positive_keyboard_key(KEYBOARD_CODE_W);

    auto x_button = input->new_axis("Left");
    x_button->set_positive_joystick_button(JOYSTICK_BUTTON_X);
    auto x_button_kbd = input->new_axis("Left");
    x_button_kbd->set_positive_keyboard_key(KEYBOARD_CODE_A);

    auto jump_button = input->new_axis("Jump");
    jump_button->set_positive_joystick_button(JOYSTICK_BUTTON_DPAD_UP);
    auto jump_button_kbd = input->new_axis("Jump");
    jump_button_kbd->set_positive_keyboard_key(KEYBOARD_CODE_SPACE);
}

void GameScene::update(float dt) {
}

void GameScene::fixed_update(float dt) {
    if (get_platform()->name() == "dreamcast")
    {
        turnSpeed = input->axis_value("Horizontal") * player->turn_speed;
        this->pitch_ -= input->axis_value("Vertical") * (player->turn_speed / player->sensitivity) * dt;
        if(input->axis_value_hard("Forward") == 1)
        {
            speed = player->speed;
        }
        else if(input->axis_value_hard("Fire1") == 1)
        {
            speed = -player->speed;
        }
        else
        {
            speed = 0;
        }

        if(input->axis_value_hard("Left") == 1)
        {
            hSpeed = -player->speed;
        }
        else if(input->axis_value_hard("Fire2") == 1)
        {
            hSpeed = player->speed;
        }
        else   
        {
            hSpeed = 0;
        }
    }
    // else if (get_platform()->name() == "psp"){
    //     turnSpeed = input->axis_value("Horizontal") * player->turn_speed * dt;
    //     if hSpeed = -1 * player->speed * dt;
    //         speed = 1 * player->speed * dt;
    //     }
    //     else if(input->axis_value_hard("X") == 1)
    //     {
    //         speed = -1 * player->speed * dt;
    //     }
    // }
    else{
        turnSpeed = input->axis_value("MouseX") * player->turn_speed;
        this->pitch_ += (input->axis_value("MouseY") * (player->turn_speed / (player->sensitivity * 50)) * dt);
        speed = input->axis_value("Vertical") * player->speed;
        hSpeed = input->axis_value("Horizontal") * player->speed;
    }
    
    this->pitch_ = smlt::clamp(this->pitch_, -85.0f, 60.0f);
    player->controller->add_force(player->controller->forward() * -speed);
    player->controller->add_force(player->controller->right() * -hSpeed);
    player->controller->add_force(player->controller->linear_velocity() * -5.0f);
    player->controller->add_torque(Vec3(0, -turnSpeed, 0));
    player->controller->add_torque(player->controller->angular_velocity() * -15.0f);


    smlt::Vec3 projected_forward = smlt::Plane(smlt::Vec3::POSITIVE_Y, 0).project(player->controller->forward());  // Project forward onto the ground plane
    projected_forward.normalize();  // Ensure length of 1
    smlt::Quaternion rot = smlt::Vec3::NEGATIVE_Z.rotation_to(projected_forward); // Get the Quaternion rotation between world "forward" and player forward"
    camera->rotate_to(rot * smlt::Quaternion(smlt::Degrees(this->pitch_), smlt::Degrees(), smlt::Degrees()));
    //camera->rotate_to(player->controller->rotation());
    camera->rotate_global_y_by(Degrees(180));

    auto maybe_hit = physics.get()->ray_cast(player->controller->position() + (Vec3::DOWN * player->actor->aabb().height() / 2) + 0.05, Vec3::DOWN, 3);
    if (maybe_hit)
    {
        gravity = 0;
        if (input->axis_value_hard("Jump") == 1)
        {
            player->controller->add_force(player->controller->up() * GRAVITY * GRAVITY);
            gravity = -GRAVITY / 2;
        }
    }
    else if (gravity < GRAVITY)
    {
        gravity += 100.0f;
    }
    else
    {
        gravity = GRAVITY;
    }

    if (!maybe_hit) player->controller->add_force(Vec3::DOWN * gravity);
    //player->controller->y += 10;
    // player->controller->add_force(-player->controller->forward() * speed * 1000);
}

void GameScene::activate() {
    // Activate any render pipelines here
}

void GameScene::deactivate() {
    // Deactviate any render pipelines here
}

void GameScene::unload() {
    // Cleanup your scene here
}
